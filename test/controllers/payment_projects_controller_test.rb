require 'test_helper'

class PaymentProjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @payment_project = payment_projects(:one)
  end

  test "should get index" do
    get payment_projects_url
    assert_response :success
  end

  test "should get new" do
    get new_payment_project_url
    assert_response :success
  end

  test "should create payment_project" do
    assert_difference('PaymentProject.count') do
      post payment_projects_url, params: { payment_project: { date_end: @payment_project.date_end, date_payment: @payment_project.date_payment, date_start: @payment_project.date_start, employee_id: @payment_project.employee_id, process: @payment_project.process, project_id: @payment_project.project_id } }
    end

    assert_redirected_to payment_project_url(PaymentProject.last)
  end

  test "should show payment_project" do
    get payment_project_url(@payment_project)
    assert_response :success
  end

  test "should get edit" do
    get edit_payment_project_url(@payment_project)
    assert_response :success
  end

  test "should update payment_project" do
    patch payment_project_url(@payment_project), params: { payment_project: { date_end: @payment_project.date_end, date_payment: @payment_project.date_payment, date_start: @payment_project.date_start, employee_id: @payment_project.employee_id, process: @payment_project.process, project_id: @payment_project.project_id } }
    assert_redirected_to payment_project_url(@payment_project)
  end

  test "should destroy payment_project" do
    assert_difference('PaymentProject.count', -1) do
      delete payment_project_url(@payment_project)
    end

    assert_redirected_to payment_projects_url
  end
end
