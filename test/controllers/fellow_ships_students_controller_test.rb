require 'test_helper'

class FellowShipsStudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fellow_ships_student = fellow_ships_students(:one)
  end

  test "should get index" do
    get fellow_ships_students_url
    assert_response :success
  end

  test "should get new" do
    get new_fellow_ships_student_url
    assert_response :success
  end

  test "should create fellow_ships_student" do
    assert_difference('FellowShipsStudent.count') do
      post fellow_ships_students_url, params: { fellow_ships_student: { bank_id: @fellow_ships_student.bank_id, cpf: @fellow_ships_student.cpf, project_id: @fellow_ships_student.project_id, student_id: @fellow_ships_student.student_id } }
    end

    assert_redirected_to fellow_ships_student_url(FellowShipsStudent.last)
  end

  test "should show fellow_ships_student" do
    get fellow_ships_student_url(@fellow_ships_student)
    assert_response :success
  end

  test "should get edit" do
    get edit_fellow_ships_student_url(@fellow_ships_student)
    assert_response :success
  end

  test "should update fellow_ships_student" do
    patch fellow_ships_student_url(@fellow_ships_student), params: { fellow_ships_student: { bank_id: @fellow_ships_student.bank_id, cpf: @fellow_ships_student.cpf, project_id: @fellow_ships_student.project_id, student_id: @fellow_ships_student.student_id } }
    assert_redirected_to fellow_ships_student_url(@fellow_ships_student)
  end

  test "should destroy fellow_ships_student" do
    assert_difference('FellowShipsStudent.count', -1) do
      delete fellow_ships_student_url(@fellow_ships_student)
    end

    assert_redirected_to fellow_ships_students_url
  end
end
