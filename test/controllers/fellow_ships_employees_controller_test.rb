require 'test_helper'

class FellowShipsEmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fellow_ships_employee = fellow_ships_employees(:one)
  end

  test "should get index" do
    get fellow_ships_employees_url
    assert_response :success
  end

  test "should get new" do
    get new_fellow_ships_employee_url
    assert_response :success
  end

  test "should create fellow_ships_employee" do
    assert_difference('FellowShipsEmployee.count') do
      post fellow_ships_employees_url, params: { fellow_ships_employee: { bank_id: @fellow_ships_employee.bank_id, cpf: @fellow_ships_employee.cpf, employee_id: @fellow_ships_employee.employee_id, project_id: @fellow_ships_employee.project_id } }
    end

    assert_redirected_to fellow_ships_employee_url(FellowShipsEmployee.last)
  end

  test "should show fellow_ships_employee" do
    get fellow_ships_employee_url(@fellow_ships_employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_fellow_ships_employee_url(@fellow_ships_employee)
    assert_response :success
  end

  test "should update fellow_ships_employee" do
    patch fellow_ships_employee_url(@fellow_ships_employee), params: { fellow_ships_employee: { bank_id: @fellow_ships_employee.bank_id, cpf: @fellow_ships_employee.cpf, employee_id: @fellow_ships_employee.employee_id, project_id: @fellow_ships_employee.project_id } }
    assert_redirected_to fellow_ships_employee_url(@fellow_ships_employee)
  end

  test "should destroy fellow_ships_employee" do
    assert_difference('FellowShipsEmployee.count', -1) do
      delete fellow_ships_employee_url(@fellow_ships_employee)
    end

    assert_redirected_to fellow_ships_employees_url
  end
end
