Rails.application.routes.draw do
  resources :payment_projects
  resources :fellow_ships_students
  resources :fellow_ships_employees
  resources :projects
  resources :editals
  resources :banks
  resources :students
  resources :employees
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
