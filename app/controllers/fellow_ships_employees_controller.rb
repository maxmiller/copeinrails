class FellowShipsEmployeesController < ApplicationController
  before_action :set_fellow_ships_employee, only: [:show, :edit, :update, :destroy]

  # GET /fellow_ships_employees
  # GET /fellow_ships_employees.json
  def index
    @fellow_ships_employees = FellowShipsEmployee.all
  end

  # GET /fellow_ships_employees/1
  # GET /fellow_ships_employees/1.json
  def show
  end

  # GET /fellow_ships_employees/new
  def new
    @fellow_ships_employee = FellowShipsEmployee.new
  end

  # GET /fellow_ships_employees/1/edit
  def edit
  end

  # POST /fellow_ships_employees
  # POST /fellow_ships_employees.json
  def create
    @fellow_ships_employee = FellowShipsEmployee.new(fellow_ships_employee_params)

    respond_to do |format|
      if @fellow_ships_employee.save
        format.html { redirect_to @fellow_ships_employee, notice: 'Fellow ships employee was successfully created.' }
        format.json { render :show, status: :created, location: @fellow_ships_employee }
      else
        format.html { render :new }
        format.json { render json: @fellow_ships_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fellow_ships_employees/1
  # PATCH/PUT /fellow_ships_employees/1.json
  def update
    respond_to do |format|
      if @fellow_ships_employee.update(fellow_ships_employee_params)
        format.html { redirect_to @fellow_ships_employee, notice: 'Fellow ships employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @fellow_ships_employee }
      else
        format.html { render :edit }
        format.json { render json: @fellow_ships_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fellow_ships_employees/1
  # DELETE /fellow_ships_employees/1.json
  def destroy
    @fellow_ships_employee.destroy
    respond_to do |format|
      format.html { redirect_to fellow_ships_employees_url, notice: 'Fellow ships employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fellow_ships_employee
      @fellow_ships_employee = FellowShipsEmployee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fellow_ships_employee_params
      params.require(:fellow_ships_employee).permit(:employee_id, :cpf, :bank_id, :project_id)
    end
end
