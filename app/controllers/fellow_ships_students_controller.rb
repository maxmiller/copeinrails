class FellowShipsStudentsController < ApplicationController
  before_action :set_fellow_ships_student, only: [:show, :edit, :update, :destroy]

  # GET /fellow_ships_students
  # GET /fellow_ships_students.json
  def index
    @fellow_ships_students = FellowShipsStudent.all
  end

  # GET /fellow_ships_students/1
  # GET /fellow_ships_students/1.json
  def show
  end

  # GET /fellow_ships_students/new
  def new
    @fellow_ships_student = FellowShipsStudent.new
  end

  # GET /fellow_ships_students/1/edit
  def edit
  end

  # POST /fellow_ships_students
  # POST /fellow_ships_students.json
  def create
    @fellow_ships_student = FellowShipsStudent.new(fellow_ships_student_params)

    respond_to do |format|
      if @fellow_ships_student.save
        format.html { redirect_to @fellow_ships_student, notice: 'Fellow ships student was successfully created.' }
        format.json { render :show, status: :created, location: @fellow_ships_student }
      else
        format.html { render :new }
        format.json { render json: @fellow_ships_student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fellow_ships_students/1
  # PATCH/PUT /fellow_ships_students/1.json
  def update
    respond_to do |format|
      if @fellow_ships_student.update(fellow_ships_student_params)
        format.html { redirect_to @fellow_ships_student, notice: 'Fellow ships student was successfully updated.' }
        format.json { render :show, status: :ok, location: @fellow_ships_student }
      else
        format.html { render :edit }
        format.json { render json: @fellow_ships_student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fellow_ships_students/1
  # DELETE /fellow_ships_students/1.json
  def destroy
    @fellow_ships_student.destroy
    respond_to do |format|
      format.html { redirect_to fellow_ships_students_url, notice: 'Fellow ships student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fellow_ships_student
      @fellow_ships_student = FellowShipsStudent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fellow_ships_student_params
      params.require(:fellow_ships_student).permit(:student_id, :cpf, :bank_id, :project_id)
    end
end
