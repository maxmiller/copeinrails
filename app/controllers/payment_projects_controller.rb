class PaymentProjectsController < ApplicationController
  before_action :set_payment_project, only: [:show, :edit, :update, :destroy]

  # GET /payment_projects
  # GET /payment_projects.json
  def index
    @payment_projects = PaymentProject.all
  end

  # GET /payment_projects/1
  # GET /payment_projects/1.json
  def show
  end

  # GET /payment_projects/new
  def new
    @payment_project = PaymentProject.new
  end

  # GET /payment_projects/1/edit
  def edit
  end

  # POST /payment_projects
  # POST /payment_projects.json
  def create
    @payment_project = PaymentProject.new(payment_project_params)

    respond_to do |format|
      if @payment_project.save
        format.html { redirect_to @payment_project, notice: 'Payment project was successfully created.' }
        format.json { render :show, status: :created, location: @payment_project }
      else
        format.html { render :new }
        format.json { render json: @payment_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payment_projects/1
  # PATCH/PUT /payment_projects/1.json
  def update
    respond_to do |format|
      if @payment_project.update(payment_project_params)
        format.html { redirect_to @payment_project, notice: 'Payment project was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment_project }
      else
        format.html { render :edit }
        format.json { render json: @payment_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payment_projects/1
  # DELETE /payment_projects/1.json
  def destroy
    @payment_project.destroy
    respond_to do |format|
      format.html { redirect_to payment_projects_url, notice: 'Payment project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_project
      @payment_project = PaymentProject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_project_params
      params.require(:payment_project).permit(:project_id, :employee_id, :process, :date_payment, :date_start, :date_end)
    end
end
