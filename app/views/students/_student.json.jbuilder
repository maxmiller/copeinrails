json.extract! student, :id, :name, :number_register, :email, :created_at, :updated_at
json.url student_url(student, format: :json)
