json.extract! edital, :id, :name, :created_at, :updated_at
json.url edital_url(edital, format: :json)
