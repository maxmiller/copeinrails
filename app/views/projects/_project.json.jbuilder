json.extract! project, :id, :name, :start, :end, :employee_id, :edital_id, :created_at, :updated_at
json.url project_url(project, format: :json)
