json.extract! fellow_ships_employee, :id, :employee_id, :cpf, :bank_id, :project_id, :created_at, :updated_at
json.url fellow_ships_employee_url(fellow_ships_employee, format: :json)
