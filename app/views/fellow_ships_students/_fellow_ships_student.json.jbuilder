json.extract! fellow_ships_student, :id, :student_id, :cpf, :bank_id, :project_id, :created_at, :updated_at
json.url fellow_ships_student_url(fellow_ships_student, format: :json)
