json.extract! payment_project, :id, :project_id, :employee_id, :process, :date_payment, :date_start, :date_end, :created_at, :updated_at
json.url payment_project_url(payment_project, format: :json)
