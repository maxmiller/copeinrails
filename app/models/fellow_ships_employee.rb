class FellowShipsEmployee < ApplicationRecord
  belongs_to :employee
  belongs_to :bank
  belongs_to :project
end
