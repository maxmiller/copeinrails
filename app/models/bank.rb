class Bank < ApplicationRecord
  validates :name, :code, presence: true
  validates :code, numericality: true

end
