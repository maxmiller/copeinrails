class FellowShipsStudent < ApplicationRecord
  belongs_to :student
  belongs_to :bank
  belongs_to :project
end
