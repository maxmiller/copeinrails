class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.date :start
      t.date :end
      t.references :employee, foreign_key: true
      t.references :edital, foreign_key: true

      t.timestamps
    end
  end
end
