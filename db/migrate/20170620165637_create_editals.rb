class CreateEditals < ActiveRecord::Migration[5.1]
  def change
    create_table :editals do |t|
      t.string :name

      t.timestamps
    end
  end
end
