class CreateFellowShipsStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :fellow_ships_students do |t|
      t.references :student, foreign_key: true
      t.string :cpf
      t.references :bank, foreign_key: true
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
