class CreatePaymentProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_projects do |t|
      t.references :project, foreign_key: true
      t.references :employee, foreign_key: true
      t.string :process
      t.date :date_payment
      t.date :date_start
      t.date :date_end

      t.timestamps
    end
  end
end
